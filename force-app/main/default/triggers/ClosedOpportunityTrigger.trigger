trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
List<Opportunity> newList= [SELECT id,stagename from Opportunity where id =:Trigger.new and stageName='Closed Won'];
    list<Task> tskList = new List<Task>();
    for (Opportunity opp : newList){
            task tsk= new Task();
            tsk.Subject='Follow Up Test Task';
            tsk.WhatId=opp.Id;
            tskList.add(tsk);
    
    }
    insert tskList;
}