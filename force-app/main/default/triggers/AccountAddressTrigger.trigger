trigger AccountAddressTrigger on Account (before insert,before update) {
        For(Account a : trigger.new){
            
            if((a.BillingPostalCode != null) && (a.Match_Billing_Address__c == true)){
             	a.ShippingPostalCode=a.BillingPostalCode;
            }
        } 

}