({
    doFormSubmit : function(component, event, helper) {
        //fetching cartypeid attribute value from event
        var carTypeId=event.getParam('carTypeId');
        console.log('cartypeId'+carTypeId);

        //calling aura method in carSearchResult
        var carSearchResultComp = component.find("carSearchResult");
        //call the method in child component
        var carSearchCmpResult = carSearchResultComp.searchCars(carTypeId);
       
    }
})