({
    doInit : function(component, event, helper) {
        //fetching car type from backgrond using apex controller
        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            component.set("v.showCardAction", true);
        } else{
            component.set("v.showCardAction", false);
        }
        helper.getCarType(component,helper);
    },

    onFullDetails : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            //console.log('in cardetail full details if tab');
                navEvt.setParams({
                "recordId" : component.get("v.car").Id,
                "slideDevName" : "detail"
            });
            navEvt.fire();
        }else{
            console.log("'e.force:navigateToSObject' event is not supported in this context");
            helper.showToast(component,{
                    "title": "Error",
                    "type" : "error",
                    "message":"Eent not supported"
            });
        }
    }
})