({
    onInit : function(component,event,helper) {
    //    var carId = component.get("v.car.Id");
        helper.callServer(component,"c.getCarExperience", 
                            function(response){
                                if(response){
                                    component.set("v.carExperiences",response);
                                }else {
                                    console.log("error getting car experience");
                                }
                            }, {
                                carId : component.get("v.car").Id
                            });

    },
})