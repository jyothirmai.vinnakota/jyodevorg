({
    doInit : function(component, event, helper) {
        helper.onInit(component, event, helper)
    },
    onSave : function(component, event, helper) {
        component.set("v.carExperience.Car__c",component.get("v.car.Id"));
        component.find("service").saveRecord($A.getCallback(function(saveResult){
            //Note: If you want a specific behaviour (an action or UI behavior) when this action is success
            //then handle that in a callback(generic logic when record is changed should be handled)
            if(saveResult.state === "SUCCESS" || saveResult.state=== "DRAFT"){
                //handle component related logic in event handler

                var resultsToast = $A.get("e.force:showToast");
                if(resultsToast){
                    resultsToast.setParams(
                        {
                            "title" : "Saves",
                            "type"   :"success",
                            "message" : "Car Experience Added"
                        });
                        resultsToast.fire();
                }
                else{
                    alert('car Experience Added');
                }
                helper.onInit(component,event,helper); // to set the blank record again

                // once ypu save the experirnce we have to see the results in car experiences ... auto refresh
                var evt=component.getEvent("carExpAdded");
                evt.fire();

            } else if(saveResult.state ==="INCOMPLETE"){
                helper.showToast(component,event,helper,{
                    "title": "ERROR!",
                    "type" : "error",
                    "message" : "Device does not support draft"
                });
                console.log('User is offline, device doesnot support draft');

            } else if(saveResult.state ==="ERROR"){
                helper.showToast(component,event,helper,{
                    "title": "ERROR!",
                    "type" : "error",
                    "message" : "Problem saving record"
                });
                console.log('Probelm saving record, error'+ JSON.stringify(saveResult.error));

            } else {
                helper.showToast(component,event,helper,{
                    "title": "ERROR!",
                    "type" : "error",
                    "message" : "Unknown Problem"
                });
                console.log('Unknow Probelm  , state'+saveResult.state+ ', error : '+ JSON.stringify(saveResult.error));
            }
        }));
    },
    onRecordUpdated :function(component,event,helper){
        var eventParams= event.getParams();
        if(eventParams.changeType === "CHANGED"){
            //get the field that changed for this record
            var changedFields = eventParams.changedFields;
            console.log('Fields that are changed :'+ JSON.stringify(changedFields));
            //record is changed, so refresh the component (or other component logic)
            helper.showToast(component,event,helper,{
                "title" : "Saved",
                "type"   : "error",
                "message" : " the record was updated"
            });
        } else if(eventParams.changeType === "LOADED"){
            //record is loaded in cache
        } else if(eventParams.changeType === "REMOVED"){
            //record is deletec and removed from the cache
        } else if(eventParams.changeType === "ERROR"){
            //there's an error while loading, saving or deleting the record
        }

    }
})