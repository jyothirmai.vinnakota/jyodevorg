({
    onSearch : function(component, event, helper) {
     var searchFormSubmit=component.getEvent("searchFormSubmit");
     searchFormSubmit.setParams({
         //get selected carTypeid from picklist
         //and pass in event attribute
         "carTypeId":component.find("carTypeList").get("v.value")
     });
     searchFormSubmit.fire();
    },
    doInit : function(component, event, helper) {
        //will fetch the cartype from backend using apex controller
        var createCarRecord = $A.get("e.force:createRecord");
        if(createCarRecord){
            component.set("v.ShowNew",true);
        }
        else{
            component.set("v.ShowNew",false);
        }
        helper.getCarType(component,helper);
        //component.set("v.carTypes",['sport', 'luxury','van','Sedan']);
        //console.log('init block');
  },
  createRecord: function(component, event, helper) {
    var createCarRecord = $A.get("e.force:createRecord");
    console.log("entered createRecord");
    createCarRecord.setParams({
        "entityApiName": "Car_Type__c"
    });
   // console.log("firing create record ");
    createCarRecord.fire();

  }
    
    //  searchList : function(component, event, helper) {
      //  var carTypeId= component.find("carTypeList").get("v.value");
       // alert('the select car is of type '+carTypeId);
      /* component.find("carTypeList").set("v.value","Sedan");
         var carTypeId1= component.find("carTypeList").get("v.value");
         console.log('the value is changed'+ carTypeId1); */
     // },
      
     // handleRender: function(component, event, helper) {
        //will fetch the cartype from backend using apex controller
       // component.set("v.carTypes",['sport', 'luxury','van','Sedan']);
     //   console.log('render block');
 // }

 /*toggleButton : function(component, event, helper) {
        var currentValue= component.get("v.isNowAvailable");
        if(currentValue){
            component.set("v.isNowAvailable",false);
            }
        else{
            component.set("v.isNowAvailable",true);
        }
      },*/

})