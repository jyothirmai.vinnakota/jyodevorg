({
	fetchListOfRecordTypes : function(component, event, helper) {
		var action = component.get("c.fetchRecordTypeValues");
		action.setParams({
			"objectName" : "Contact"
		});

		action.setCallback(this, function(response){
			var maplist = response.getReturnValue();
			component.set("v.mapOfRecordType",maplist);
			var recordTypeList=[];
			//component.set("v.recordTypeList",maplist[key]);
			//console.log('in call back'+{v.recordTypeList});
			//cretaing list from map
			for(var key in maplist){
		//	console.log('in for loop'+maplist(key));
			recordTypeList.push(maplist[key]);
				console.log(maplist[key]);
			}
			if(recordTypeList.length ==0){
				helper.closeModal();
				helper.showCreationRecordModal(component,"","Contact");
			} else{
				console.log('in else before setting list');
				component.set("v.lstOfRecordType",recordTypeList);
			} 
		});
		$A.enqueueAction(action);
	},
	createRecord  : function(component, event, helper) {
		var SelectedRecordTypeName = component.find("recordTypePickList").get("v.value");
		if(SelectedRecordTypeName != ""){
			var selectedRecordTypeMap = component.get("v.mapOfRecordType");
			var selectedRecordTypeId;

			for(var key in selectedRecordTypeMap){
				if(SelectedRecordTypeName == selectedRecordTypeMap[key]){
					selectedRecordTypeId = key;
					break;
				}
			}
			//closing quick action modal here
			helper.closeModal();

			//calling createrecord modal here without providing recordtypeId
			helper.showCreateRecordModal(component,selectedRecordTypeId,"Contact");
		} else{
			alert('you didnt select any record type');
		}
	},

	closeModal: function(component,event,helper) {
		helper.closeModal();
	}
})