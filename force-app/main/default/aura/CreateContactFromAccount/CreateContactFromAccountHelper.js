({
	showCreateRecordModal : function(component,recordTypeId,entityApiName) {
		var createRecordEvent = $A.get("e.force:createRecord");
		if(createRecordEvent){
			if(recordTypeId){
				createRecordEvent.setParams({
					"entityApiName" : entityApiName,
					"recordTypeId"  : recordTypeId,
					"defaultFieldValues" : {
						"AccountId" : component.get("v.recordId")
					}
				});
			} else {
				createRecordEvent.setParams({
					"entityApiName" : entityApiName,
					"defaultFieldValues" : {
						"AccountId" : component.get("v.recordId")
					}
				});
					
				
			}
			createRecordEvent.fire();
		} else{
			alert('This event is not supported');
		}
	
	},

	closeModal : function() {
		var closeEvent= $A.get("e.force:closeQuickAction");
		if(closeEvent){
			closeEvent.fire();
		} else{
			alert('force:closeQuickAction event isnot supported in this lightnong context');
		}
	}
})