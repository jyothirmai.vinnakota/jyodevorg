({
	jsLoaded: function(component) {
        component.set("v.jsLoaded", true);
    },  
    
    loadMap: function(component, event, helper) {
    	debugger;
        var car = event.getParam('car');
        var latitude = car.Geolocation__latitude__s;
    	var longitude = car.Geolocation__longitude__s;
        
        component.set("v.location", {'lat' : latitude, 'long' : longitude});
    },
})