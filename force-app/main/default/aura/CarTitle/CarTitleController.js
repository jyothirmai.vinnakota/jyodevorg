({
    onCarClick : function(component, event, helper) {
        var car= component.get("v.car");

        /*Create carselect event and pass required carid 
        fire the event so that the parent carSearchResult component
        can handle the event.
        */
   
       var evt= component.getEvent("onCarSelect");
       evt.setParams({
           "carId" : car.Id
       });
       evt.fire();

       /* firing Application event */
       var appEvent = $A.get("e.c:CarSelectedApplicationEvent");
      
       if(appEvent){
        console.log('in application event if');
           appEvent.setParams({
               "car" : car
           });
           console.log('before fire application event');
           appEvent.fire();   
          }
           
    }
})