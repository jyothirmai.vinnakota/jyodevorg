public with sharing class CarTypeSearch {
    @AuraEnabled
    public static List<Car_Type__c> getCarType(){
        return[Select Id,Name from Car_Type__c];
        
    }
}