public class ContactSearch {
    public static Contact[] searchForContacts(String para1, String para2){
        
        Contact[] cts = [SELECT ID, Name FROM Contact where lastName =:para1 AND MailingPostalCode =:para2];
        return cts;
    }
}