public with sharing class ContactRecordTypeSelector {
    public static Map<Id,String> recordtypemap;
    @AuraEnabled
    public static Map<Id,String> fetchRecordTypeValues(String objectName){
        List<Schema.RecordTypeInfo> recordTypes = Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfos();
        recordtypemap = new Map<Id,String>();
        for(RecordTypeInfo rt : recordTypes){
            if(rt.getName() != 'Master' && rt.getName().trim()!= '')
            recordtypemap.put(rt.getRecordTypeId(), rt.getName());
        }
        System.debug('before return');
        return recordtypemap;
    }
   
}