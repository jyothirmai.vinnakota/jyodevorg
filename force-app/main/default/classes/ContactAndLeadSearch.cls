public class ContactAndLeadSearch {
    
    public static List<List<SObject>> searchContactsAndLeads(String para){
        List<List<SObject>> searchList = [FIND :para IN NAME FIELDS RETURNING Contact,Lead ]; 
        return searchList;
    }
}