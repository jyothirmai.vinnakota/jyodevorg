@isTest
public class TestRestrictContactByName {
    @isTest static void InsertwithInvalidName(){
        contact cont = new Contact(Lastname='INVALIDNAME');
        Test.startTest();
        database.SaveResult result = Database.insert(cont,false);
        Test.stopTest();
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML',
                             result.getErrors()[0].getMessage());
        
    }
    @isTest static void insertProperName(){
        contact cont = new Contact(Lastname='jyo');
        Test.startTest();
        database.SaveResult result = Database.insert(cont);
        Test.stopTest();
        System.assert(result.isSuccess());
         
    }

}