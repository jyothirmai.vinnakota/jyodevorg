public with sharing class CarExperience {
  
  @AuraEnabled
  public static List<Car_Experience__c> getCarExperience(Id carId){
      return [SELECT Id,
                     Name,
                     Experience__c,
                     LastModifiedDate,
                     CreatedDate,
                     CreatedBy.Name,
                     CreatedBy.SmallPhotoUrl,
                     CreatedBy.CompanyName 
                     FROM Car_Experience__C 
                     WHERE Car__C =:carId 
                     ORDER BY CreatedDate DESC];
      
  }
}